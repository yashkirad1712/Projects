
import pyttsx3
import datetime
import speech_recognition as sr
import wikipedia
import webbrowser
import smtplib


engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
engine.setProperty('voice', voices[0].id)


def speak(audio):
    engine.say(audio)
    engine.runAndWait()

def wishMe():
    hour = datetime.datetime.now().hour
    if 0 <= hour < 12:
        speak("Good Morning!")
    elif 12 <= hour < 18:
        speak("Good Afternoon!")
    else:
        speak("Good Evening!")

    speak("I am Jarvis Sir. Please tell me how may I help you")

def takeCommand():
    r = sr.Recognizer()
    with sr.Microphone() as source:
        print("Listening...")
        r.pause_threshold = 1
        audio = r.listen(source)

    try:
        print("Recognizing...")
        query = r.recognize_google(audio, language='en-in')
        print(f"User Said: {query}\n")

    except sr.UnknownValueError:
        print("Sorry, I did not understand. Please repeat.")
        return "none"
    except sr.RequestError as e:
        print(f"Could not request results from Google Speech Recognition service; {e}")
        return "none"

    return query

def sendEmail(to, content):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('yashkirad622002@gmail.com', 'yankykilo6')  # Replace with your actual password
    subject = 'Subject: Your Subject Here\n\n'  # Add a subject for the email
    email_body = subject + content
    server.sendmail('yashkirad622002@gmail.com', to, email_body)
    server.close()


if __name__ == '__main__':
    wishMe()
    while True:
        query = takeCommand().lower()

        if 'wikipedia' in query:
            speak('Searching Wikipedia...')
            query = query.replace("wikipedia", "")
            results = wikipedia.summary(query, sentences=2)
            speak("According to Wikipedia")
            speak(results)
        elif 'open youtube' in query:
            speak("Opening Youtube Sir")
            webbrowser.open("https://www.youtube.com/")

        elif 'open google' in query:
            speak("Opening google sir")
            webbrowser.open("https://www.google.com/")

        elif 'open linkedin' in query:
            speak("Opening LinkedIn sir")
            webbrowser.open("https://www.linkedin.com/in/yash-kirad-006075250/")

        elif 'open gmail' in query:
            speak("Opening gmail sir")
            webbrowser.open("https://mail.google.com/mail/u/0/#inbox")

        elif 'the time' in query:
            strTime = datetime.datetime.now().strftime("%H:%M:%S")
            speak(f"sir, the time is {strTime}")

        elif 'email to yash' in query:
            speak("Not given the access yet sir")
            try:
                speak("What should I Say?")
                content = takeCommand()
                to = "yashkirad.rmdssoe.comp@gmail.com"
                sendEmail(to, content)
                speak("Email has benn Delivered Successfully")
            except Exception as e:
                print(e)
                speak("Sorry my friend lagta hai email nahi gaya")

        elif 'sleep' in query:
            speak("Getting Deactivated   see you when  activated")
            exit()