import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class QR_Generate {

    public static void main(String[] args) {
        // Assuming GoogleForm is a class representing the form details
        GoogleForm form = getGoogleFormDetails(); // You need to implement this method

        // Generate QR code
        String content = form.toString(); // Convert form details to a string
        String filePath = "output.png"; // Output file path

        try {
            generateQRCode(content, filePath);
            System.out.println("QR Code generated successfully.");
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
    }

    private static void generateQRCode(String content, String filePath) throws WriterException, IOException {
        int width = 300;
        int height = 300;

        Map<EncodeHintType, Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");

        BitMatrix matrix = new QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(matrix, "PNG", path);
    }

    // Example GoogleForm class (you should replace this with your actual implementation)
    private static class GoogleForm {
        private String name;
        private String email;

        public GoogleForm(String name, String email) {
            this.name = name;
            this.email = email;
        }

        @Override
        public String toString() {
            return "Name: " + name + "\nEmail: " + email;
        }
    }

    private static GoogleForm getGoogleFormDetails() {
        // Replace this with logic to get form details from the Google Form
        return new GoogleForm("John Doe", "john.doe@example.com");
    }
}
