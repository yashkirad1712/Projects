CREATE DATABASE VersionVerse;
USE VersionVerse;

CREATE TABLE Lang_Name (
    Id INT PRIMARY KEY AUTO_INCREMENT,
    LanguageName VARCHAR(255) NOT NULL
);

INSERT INTO Lang_Name (LanguageName) VALUES ('Java');

CREATE TABLE Versions (
   Id INT PRIMARY KEY AUTO_INCREMENT,
    LanguageId INT,
    LanguageVersion VARCHAR(255) NOT NULL,
    HtmlLinks TEXT NOT NULL,
    FOREIGN KEY (LanguageId) REFERENCES Lang_Name(Id)
);

INSERT INTO Versions (LanguageId, LanguageVersion, HtmlLinks) VALUES
(1, 'Java-JDK1.0', 'Y:\\Projects\\Demo_Database\\Java\\JDK1.0.html'),
(1, 'Java-JDK1.1', 'Y:\\Projects\\Demo_Database\\Java\\JDK1.1.html'),
(1, 'Java-J2SE 1.2', 'Y:\\Projects\\Demo_Database\\Java\\J2SE 1.2.html'),
(1, 'Java-J2SE 1.3', 'Y:\\Projects\\Demo_Database\\Java\\J2SE 1.3.html'),
(1, 'Java-J2SE 1.4', 'Y:\\Projects\\Demo_Database\\Java\\J2SE 1.4.html'),
(1, 'Java-SE 5', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 5.html'),
(1, 'Java-SE 6', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 6.html'),
(1, 'Java-SE 7', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 7.html'),
(1, 'Java-SE 8', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 8.html'),
(1, 'Java-SE 9', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 9.html'),
(1, 'Java-SE 10', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 10.html'),
(1, 'Java-SE 11', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 11.html'),
(1, 'Java-SE 12', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 12.html'),
(1, 'Java-SE 13', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 13.html'),
(1, 'Java-SE 14', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 14.html'),
(1, 'Java-SE 15', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 15.html'),
(1, 'Java-SE 16', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 16.html'),
(1, 'Java-SE 17', 'Y:\\Projects\\Demo_Database\\Java\\Java SE 17.html');


SELECT * FROM Versions;